<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public $customer;

	/**
	* Get All Data from this method.
	*
	* @return Response
	*/

	public function __construct() {
		parent::__construct();
		$this->load->model('Customer_model');
		$this->customer = new Customer_model;
	}

	public function index()
	{
		$this->lang->load('customer_lang');
		$data['subview'] = 'customers/index';
		$this->load->view('layout', $data);
	}

	public function lists() {
		$this->lang->load('customer_lang');
		$config['full_tag_open']        = '<nav><ul class="pagination">';
        $config['full_tag_close']       = '</ul></nav>';
        $config['num_tag_open']     = '<li class="page-item">';
        $config['num_tag_close']        = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><a class="page-link notClick" href="#">';
        $config['cur_tag_close']        = '<span class="sr-only">(current)</span></a></li>';
        $config['prev_tag_open']        = '<li>';
        $config['prev_tag_close']       = '</li>';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['first_link']           = '&laquo;';
        $config['prev_link']            = '&lsaquo;';
        $config['last_link']            = '&raquo;';
        $config['next_link']            = '&rsaquo;';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['attributes'] = array('class' => 'page-link pageLink');
        $config['base_url']         = base_url('');
        $config['use_page_numbers'] = TRUE;

        $config['per_page']         = 2;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment']= 'page';
        $page = (empty($_GET['page']) || !intval($_GET['page'])) ? 1 : $_GET['page'];
        list($config['total_rows'], $data['data']) = $this->customer->get_lists($page, $config['per_page']);
        $this->pagination->initialize($config);

        return $this->load->view('customers/lists', $data);

	}

	public function store($id = 0) {
		// set validation rules	
		$this->form_validation->set_rules($this->customer->rules); 
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			// create new record and then redirect to index
			$postData = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email')
			);
			if ($id) {
				$this->customer->update($postData, $id);
			} else {
				$this->customer->insert($postData);
			}

			echo json_encode(array("statusCode" => TRUE));
		} else {
			$arr_messages = array(
				'name' => form_error('name', '<div class="errorMessage alert alert-danger">', '</div>'),
				'email' => form_error('email', '<div class="errorMessage alert alert-danger">', '</div>')
			);
			echo json_encode(array("statusCode" => FALSE, 'mes' => $arr_messages));
		}
	}

	public function edit($id) {
		$data = $this->customer->get_by_id($id);
		echo json_encode($data);
	}

	public function delete($id) {
		$this->customer->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
}
