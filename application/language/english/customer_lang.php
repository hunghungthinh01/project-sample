<?php
$lang['create_customer'] = 'Create Customer';
$lang['list_customer'] = 'List Customers';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['action'] = 'Action';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['not_data'] = 'No Customers';
$lang['cancel'] = 'Cancel';
$lang['submit'] = 'Submit';
