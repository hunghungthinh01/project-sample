<?php

class Customer_model extends CI_Model {
    public $rules = array(
        'name' => array(
        'field' => 'name', 
        'label' => 'Name', 
        'rules' => 'trim|required'
        ),
        'email' => array(
        'field' => 'email', 
        'label' => 'Email', 
        'rules' => 'trim|required|valid_email'
        )
     );

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_lists($page, $per_page) {
        $total_rows = $this->db->get("customers")->num_rows();
        $query = $this->db->get("customers", $per_page, ($page-1) * $per_page);
        return [$total_rows, $query->result()];
    }

    /*
     * Insert post
     */
    public function insert($data = array()) {
        $insert = $this->db->insert('customers', $data);
        if($insert) {
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    /*
     * Update post
     */
    public function update($data, $id) {
        if(!empty($data) && !empty($id)) {
            $update = $this->db->update('customers', $data, array('id' => $id));
            return $update ? true : false;
        }else{
            return false;
        }
    }
    
    /*
     * Delete post
     */
    public function delete_by_id($id) {
        $delete = $this->db->delete('customers',array('id' => $id));
        return $delete ? true : false;
    }

    public function get_by_id($id)
    {
        $this->db->from('customers');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }
}
