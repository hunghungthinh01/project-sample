<div class="getLists">
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <h3 class="modal-title"><?= lang('create_customer'); ?></h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>
  <div class="modal-body form">
    <form action="<?php echo base_url('customers'); ?>" id="formCreateOrUpdate" class="form-horizontal">
      <input type="hidden" value="" name="id"/>
      <div class="form-body">
        <div class="form-group">
          <label class="control-label col-md-3"><?= lang('name'); ?></label>
          <div class="col-md-9">
            <input name="name"  class="form-control" type="text">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?= lang('email'); ?></label>
          <div class="col-md-9">
            <input name="email" class="form-control" type="text">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn-edit" class="btn btn-primary"><?= lang('submit'); ?></button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><?= lang('cancel'); ?></button>
      </div>
    </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script type="text/javascript">
    var baseUrl = "<?php echo base_url(); ?>";
    var isFirstTime = true;
</script>
