<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2><?= lang('list_customer'); ?></h2>
            <?php echo $this->pagination->create_links(); ?>
        </div>
        <div class="pull-right">
            <a class="btn btn-success createNew" data-toggle="modal" data-target="#modal_form" href="#"> <?= lang('create_customer'); ?> </a>
        </div>
    </div>
</div>
<?php if (count($data)) { ?>
<table class="table table-bordered">
  <thead>
      <tr>
          <th><?= lang('name'); ?></th>
          <th><?= lang('email'); ?></th>
          <th width="220px"><?= lang('action'); ?></th>
      </tr>
  </thead>

  <tbody>
   <?php foreach ($data as $item) { ?>
      <tr>
          <td id="name-<?php echo $item->id; ?>"><?php echo $item->name; ?></td>
          <td id="email-<?php echo $item->id; ?>"><?php echo $item->email; ?></td>
          <td>
            <form class="deleteForm" method="post" action="<?php echo base_url('customers/'.$item->id.'/delete');?>">
             <a class="btn btn-primary editItem" href="<?php echo base_url('customers/'.$item->id.'/edit'); ?>" data-redirect="<?php echo base_url('customers/'.$item->id.'/update'); ?>"> <?= lang('edit'); ?></a>
              <button type="submit" class="btn btn-danger"> <?= lang('delete'); ?></button>
            </form>
          </td>
      </tr>
      <?php } ?>
  </tbody>
</table>
<?php } else { ?>
    <?= lang('not_data'); ?>
<?php } ?>
