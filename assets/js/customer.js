$(document).on('click','.editItem', {} ,function(event){
  event.preventDefault();
  var url = $(this).attr('href');
  var redirectToUrl = $(this).attr('data-redirect');
    $.ajax({
       type: "GET",
       url: url,
       success: function(res)
       {
            var data = JSON.parse(res);
            resetErrorMessages();
            ['id', 'name', 'email'].map( function(item) {
                $( "input[name=" + item +"]" ).val(data[item]);
            });
            $('#modal_form').modal(); // display form edit
       }
    });
    $('#formCreateOrUpdate').attr('action', redirectToUrl);
});

$(document).on('click','#btn-edit', {} ,function(event){
    var data = $("#formCreateOrUpdate").serialize();
    var url = $("#formCreateOrUpdate").attr('action');
    createOrUpdateByAjax(url, data); // call function create item
});


$(document).on('click','.createNew', {} ,function(event){
    $("#formCreateOrUpdate").trigger('reset');
    $('#formCreateOrUpdate').attr('action', baseUrl + "/customers");
});

$(document).on('click','.pageLink', {} ,function(event){
    event.preventDefault();
    if ($(this).hasClass('notClick')) {
        return false;
    }
    var url = $(this).attr('href');
    getLists(true, url);
    return false;
});

$( ".deleteForm" ).submit(function( event ) {
  event.preventDefault();
  var url = $(this).attr('action');
  var result = confirm("Want to delete?");
  if (result) {
     $.ajax({
       type: "POST",
       url: url,
       success: function(res)
       {
            var data = JSON.parse(res);
            location.reload();
       }
    });
  }
});

// $( "#formCreateOrUpdate" ).submit(function( event ) {
//   event.preventDefault();
//   var data = $("#formCreateOrUpdate").serialize();
//   var url = $("#formCreateOrUpdate").attr('action');
//   createOrUpdateByAjax(url, data); // call function create item
// });

function createOrUpdateByAjax(url, data) {
    $.ajax({
       type: "POST",
       url: url,
       data: data, // serializes the form's elements.
       success: function(res)
       {
            var data = JSON.parse(res);
            console.log(data);
            if (!data.statusCode) {
                resetErrorMessages();
                setErrorMessages(data.mes);
            } else {
                var id = $('input[name="id"]').val();
                if(id) {
                    $("#name-" + id).html($('input[name="name"]').val());
                    $("#email-" + id).html($('input[name="email"]').val());
                } else {
                    
                }
            }
       }
    });
}

function resetErrorMessages() {
    $('.errorMessage').remove();
}

function setErrorMessages(data) {
    $('.modal-body').prepend("<div class='displayAlerts errorMessage alert alert-danger'></div>");
    ['name', 'email'].map( function(item) {
     $( ".displayAlerts" ).prepend(data[item]);
     $( "input[name=" + item +"]" ).after(data[item]);
    });
}

function getLists(skip, url, page) {
    if (!skip) {
        if (!url) url = baseUrl + 'customers/lists';
        if (typeof page !== 'undefined' || null == page) {
            url = url + '?page=' + page;
        }
    }
    page = url.substring(url.indexOf("=")+1);
    //
    $.ajax({
       type: "GET",
       url: baseUrl + 'customers/lists' + '?page=' + page,
       success: function(res)
       {
            $('.getLists').html(res);
       }
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(document).ready(function(){
    var calPage = getParameterByName('page');
    if(isFirstTime) {
        getLists(false, baseUrl + 'customers/lists', calPage);
    }
    isFirstTime = false;
});
